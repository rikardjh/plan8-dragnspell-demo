/* globals module */

module.exports = function(grunt) {

    // Project configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        watch: {
            
            scripts: {
                files: ['src/**/*.js', 'tests/**/*.js', 'src/**/*.scss'],
                tasks: ['jshint', 'mocha_phantomjs', 'sass']
            },

            options: {
              livereload: true,
            }
        },

        jshint: {
            options: {
                laxbreak: true
            },
            all: ['Gruntfile.js', 'src/**/*.js' ]
        },

        urequire:{
            myLibAsUMD: {
                template: "UMD", // default, can be ommited
                path: "src/umdtest/",
                dstPath: "build/umdLibtest"
            },

            myLibCombinedToWorkEverywhere: {
                template:'combined',
                path: "src/umdtest/",
                main: 'src/umdtest/main',
                dstPath: "build/combinedLib.js"
            },

            _defaults: {
                debugLevel:90,
                verbose: true,
                scanAllow: true,
                allNodeRequires: true,
                noRootExports: false
            }
        },

        concat: {

            options: {
                separator: ';'
            },

            clean : {

                src: [
                    "<%= meta.licenseFile %>",
                    "<%= meta.srcFiles %>"
                ],

                dest: 'build/jsmax.clean.js'
            },

            dist: {

                src: [
                    '<%= meta.licenseFile %>',
                    '<%= meta.depFiles %>',
                    '<%= meta.srcFiles %>'
                ],

                dest: 'build/<%= pkg.name %>'
            }
        },

        connect: {
            server: {
                options: {
                    keepalive: true,
                    port: 3000
                }
            }
        },

        // mocha: {
        //   test: {
        //     src: ['tests/**/*.html'],
        //   },
        // },

        mocha_phantomjs: {
            all: ['tests/**/*.html']
        },

        sass: {
            dist: {
                files: {
                    'build/css/main.css': 'src/scss/main.scss'
                }
            }
        },

    });

    // Load tasks
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-mocha-phantomjs');
    
    // Default task
    grunt.registerTask('default', ['mocha_phantomjs', 'sass']);

};