#Drag'N'Play#
#====================#
This demo plays an audio voice recording at variable speed as the mouse is moved over text element.

### Todo ###

*  Map text to sound more closely
*  Write more tests for phase vocoder
*  Phase Vocoder needs some work, especially frame windowing on output signal

### Goals ###

**  Given an voice recording  and a text representation of the same:
    Play the voice recording with variable speed when mouse moves over the text
    The mapping can be 1-1 between recording and text (i.e X% of text width maps to X% of recording) (done)

*   Given an voice recording  and a text representation of the voice recording:
    Allow for manually mapping points (in samples) in audio file to be played back individually as slices
    Allow for playing back these points as the user moves the mouse over the text

*   Make the voice recording play back continueslly (repeated) as the mouse moves over the text (i.e. granular synthesis or similar)

*   Adopt to be able to handle dynamic text. Use google TTS to generate audio from text, analyze the result and dynamically map phonetic content to points in audio. 

*   Make it pretty ;)


### Approaches / Ideas ###

*   Time-stretching:

    *   Native time-stretching: Many modern browsers has support for variable playback speed (constant pitch) on audio elements. Not possible on audiobuffers yet though. 
        * See **spikes/scrubbing_audio_element.html** for working example
    *   Granular-synth: See example <a href="http://chromium.googlecode.com/svn/trunk/samples/audio/granular.html">here</a>
    *   Phase-vocoding: FFT based time-stretching could be achived with a scriptProcessorNode. Harder