define( [ 'jquery', './lib/ResourceLoaderAudio', './lib/PhaseVocoder' ],
    
    function ( $, ResourceLoaderAudio, PhaseVocoder ) {

    'use strict';
    
    return {
        
        initialize : function ( el ) {

            this.$el    = $( el );
            this.$text  = $( '<div><h1 id="main-text"><span class="word" data-count="1">hello</span> <span class="word" data-count="2">world</span></h1></div>' );

            // native bind, we're running modern browsers ;)
            this.$words     = this.$text.find( '.word' );
            this.wordCount  = this.$words.length;

            this.$words.on( 'mousemove', this._onMouseMove.bind( this ) );

            var loader = new ResourceLoaderAudio();
            var audioContext = this._audioContext = new AudioContext();

            var _this = this;
            loader.load( [ 'sounds/hello_world_trimmed.m4a' ], audioContext, function ( hello_world ) {
  
                var duration = hello_world.duration;

                var framesNeeded = PhaseVocoder.durationToFrames( duration, audioContext.sampleRate, 1024, 512 );
                var phaseVocoder = new PhaseVocoder( audioContext, framesNeeded, 1024, 512 );
                
                phaseVocoder.createSTFT( hello_world.getChannelData( 0 ) );

                // !important keep a reference to the phase vocoder alive, otherwise
                //  the scriptprocessors will be garbage collected
                _this.phaseVocoder = phaseVocoder;
                
                // play, but set speed to 0.. the magic of phase vocoders :)
                phaseVocoder.play( 0, true );
                phaseVocoder.connect( audioContext.destination );

                _this._setupAnalyser();
                _this.render(); 
                
            } );

          
        },

        render : function () {
            this.$text.detach(); // preserve event bindings
            this.$el.html( '' );
            this.$el.append( this.$text );
            this.$el.append( this.$visualizer );
            return this;
        },

        _onMouseMove : function ( e ) {
            var $target = $( e.target );
            var pos     = parseInt( $target.data( 'count' ) );
            var offset  = $target.offset();
            var relX    = e.pageX - offset.left;
            var width   = $target.width();
            var ratioX  = relX / width;
            var posRatio = 1 / this.wordCount;
            var start   = pos * posRatio - posRatio;
            
            // offset fix for this particular demo
            start += -0.01;

            this.phaseVocoder.progress( start + ratioX * posRatio );
        },

        _setupAnalyser : function () {
            var analyser = this._audioContext.createAnalyser();
            this.phaseVocoder.connect( analyser );
            
            var WIDTH  = 320;
            var HEIGHT = 200;
            this.$visualizer = $( '<canvas width='+WIDTH+' height='+HEIGHT+'>' );
            
            var drawContext = this.$visualizer[0].getContext( '2d' );

            ( function drawLoop () {
                drawContext.clearRect( 0, 0, WIDTH, HEIGHT );
                var freqDomain = new Uint8Array(analyser.frequencyBinCount);
                analyser.getByteFrequencyData(freqDomain);
                for (var i = 0; i < analyser.frequencyBinCount; i++) {
                    var value = freqDomain[i];
                    var percent = value / 256;
                    var height = HEIGHT * percent;
                    var offset = HEIGHT - height - 1;
                    var barWidth = WIDTH/analyser.frequencyBinCount;
                    //var hue = i/analyser.frequencyBinCount * 360;
                    //drawContext.fillStyle = 'hsl(' + hue + ', 100%, 50%)';
                    drawContext.fillStyle = '#999'; 
                    drawContext.fillRect( i * barWidth, offset, barWidth, height );
                }

                requestAnimationFrame( drawLoop );
            } ());

            
        }
    };
});