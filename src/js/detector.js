// Detect browser support
define( function () {

    'use strict';
    
    // add any requirements to test
    function test () {
        var err = [];

        if ( err.length === 0 ) {
            return true;
        } else {
            console.error( 'detect errors: ', err );
            return err;
        }
    }


    return {
        test : test
    };
});