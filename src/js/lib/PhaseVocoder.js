/**
*   A quick and dirty phase vocoder for time-stretching. More of a proof-of-concept at this point
*
*   Dependencies:
*       PhaseVocoder needs a fft forward and reverse (ifft) function. A slightly modified version of DSP.js 
*       is used for now and should be exposed as FFT on root or as a module named the same. 
* 
*   If not working with the WebAudio API is required (AudioBuffers etc.)
*   Look at the Audio Elements playbackRatio instead. Much more efficient, but currently only for audio elements
*
*   Todo:
*   * Explore if an analysernode could be used to handle atleast fft forward nativly
*     Though, since it's only used non-realtime it might not be worth it.
*     (I suspect that fft from analysernode uses the Nyquist frequency and thus 
*     the stft algorithm must be adapted)
*   * Try different window shapes, for now only Hanning window is used
*   * Optimize polar<->cartecian conversion using complex math as described in this article:
*       http://cycling74.com/2006/11/02/the-phase-vocoder-%E2%80%93-part-i/
*   * !!! No proper windowing between frames takes place on playback.
*     Instead a simple interpolation between current and previous output is used. While it does get rid of most clicks / crackles
*     a proper windowing function would give a much smoother sound
*
*   @author Rikard Lindstrom
*/ 

/* universal module definition */
( function ( root, factory ) {
    if (typeof define === 'function' && define.amd) {
        define([ 'FFT' ], factory);
    } else if ( typeof exports === 'object' ) {
        module.exports = factory( require( 'FFT' ), require( 'underscore' ));
    } else {
        root.PhaseVocoder = factory( root.FFT );
    }
}( this, function ( FFT ) {

        'use strict';

        var HALF_PI = 0.5 * Math.PI;
        var TWO_PI  = 2 * Math.PI;

        /* Utility functions to convert between polar / cartesian coordinates */
        function carToPol( x, y ) {
            var r     = Math.sqrt( x * x + y * y );
            var theta = HALF_PI - Math.atan2( x, y ); 
            return [ r, theta  ];
        }

        function polToCar( r, theta ) {
            return [ r * Math.cos( theta ), r * Math.sin( theta ) ];
        }

        /*  Wrap between -PI and PI, not optimal (creates floating point errors) 
        *   Inlined at most places (remove?)
        */
        function phaseWrap( phase ) {
            return phase - 2 * Math.PI * Math.floor( phase / ( TWO_PI ) + 0.5 ); 
        }

        /**   
        *   @constructor
        *   @param {AudioContext} audioContext
        *   @param {integer} frames Number of fft frames to use
        *   @param {integer} windowSize
        *   @param {integer} hopSize Spacing between windowframes (should be less than windowSize)
        */
        function PhaseVocoder( audioContext, frames, windowSize, hopSize ) {

            hopSize = hopSize  || windowSize / 2;


            if ( hopSize !== windowSize / 2 ) {
                console.warn( 'PhaseVocoder: a hop size not equal to half window '+
                              'size is not supported correctly now (playback inte'+
                              'rpolation is based on 2 frames)');
            }

            this._windowSize    = windowSize;
            this._hopSize       = hopSize;
            this._numFrames     = frames;
            this._currentFrame  = 0;
            this._audioContext  = audioContext;
            this._currenWindow  = 0;
            // buffers to hold the windowed fft frames (in polar cordinates)
            this._phaseBuff     = new Float32Array( windowSize * frames );
            this._ampBuff       = new Float32Array( windowSize * frames );


            // playback fft object
            this._fft           = new FFT( windowSize, audioContext.sampleRate );

            this._record        = false;
            this._play          = false;

            // Todo: move into rec initialization (do not create here)
            this._recBuff       = audioContext.createBuffer( 1, frames * hopSize + windowSize, audioContext.sampleRate );

            // this could be extracted to allow for different window types
            this.hanningWindow = ( function () {
                var hanningWindow = new Float32Array( windowSize );
                var j;
                for ( j = windowSize - 1; j >= 0; j--) {
                    hanningWindow[ j ] = 0.5 + ( 0.5 * Math.cos( Math.PI + TWO_PI * ( j / windowSize ) ) );
                }

                return hanningWindow;
            } () );

          //  this._initRecNode();
            this._initPlayNode();
            this.playbackRate  = 1;

            
        }

        /**
        *   Initialize playback scriptprocessor
        *   The scriptprocessing handles the second stage of the phasevocoding (interpolating frames, calculate running phase and reverse ftt)
        *   @private
        */
        PhaseVocoder.prototype._initPlayNode = function () {
            var _this       = this;
            var ctx         = this._audioContext;
            var windowSize  = this._windowSize;
            var numFrames   = this._numFrames;
            var hopSize     = this._hopSize;
            var phaseBuff   = this._phaseBuff;
            var ampBuff     = this._ampBuff;
            var fft         = this._fft;
            var realOut     = new Float32Array( windowSize );
            var imagOut     = new Float32Array( windowSize );
            var overlapFactor = windowSize / hopSize;
            var hanningWindow = this.hanningWindow;
            var frameAccum  = new Float32Array( windowSize );
            var lastOutput  = new Float32Array( windowSize );

            // init scriptprocessor
            var playNode    = ctx.createScriptProcessor( windowSize, 1, 1 );
            playNode.onaudioprocess = function ( e ) {
                var output = e.outputBuffer.getChannelData( 0 );

                if ( _this._play && !_this._record ) {

                    var currentFrame = Math.max( 0, Math.floor( _this._currentFrame ) ); 
                    var framePosition =  currentFrame * windowSize;

                    // reverse the polar coordinates to cartesian and calculate the running phase over current and previous frame
                    // we use window size for iteration here, but in reality it's the bin size (which always equals windows size)
                    for ( var i = 0; i < windowSize; i++ ) {

                        var amp   = ampBuff[ framePosition + i ];
                        var phase = phaseBuff[ framePosition + i ];
                        frameAccum[ i ] += phase;
                        
                        for ( var j = 1; j < overlapFactor; j++ ){
                            // sanity check / hack.. we don't want to go back further than the first frame
                            // we could also make sure to set the currentFrame to >= overlapfactor
                            if ( framePosition > hopSize * j ) {
                                frameAccum[ i ] += phaseBuff[ ( framePosition - hopSize * j ) + i ];
                            }
                        }

                        // add some dithering?
                        //frameAccum[ i ] += Math.random() * 1 - 2;

                        // wrap phase, this could be improved a lot (lots of rounding errors)
                        frameAccum[ i ] = frameAccum[ i ] - TWO_PI * Math.floor( frameAccum[ i ] / TWO_PI + 0.5 );
                        
                        var cart = polToCar( amp, frameAccum[ i ] );
     
                        realOut[ i ] = cart[ 0 ];
                        imagOut[ i ] = cart[ 1 ];
                        
                    }
                    
                    // back to time domain
                    fft.inverse( realOut, imagOut, output );

                    // a poor interpolation technique which discards a lot of work done before this point
                    // Only based on the previous frame and will give a very robotic sound
                    for ( i = 0; i < windowSize; i++ ) {
                        output[ i ] = output[ i ] * hanningWindow[ i ];

                        // cashe for interpolation
                        lastOutput[ i ] = output[ i ];

                        // random interpolation
                        output[ i ] += lastOutput[ ( ( ( windowSize - hopSize ) + i ) % windowSize ) ];
                        output[ i ] *= 0.5;
                    }

                    _this._currentFrame += _this.playbackRate * overlapFactor;

                    // wrap playback frame 
                    if( _this._currentFrame > numFrames -2 ) {
                        _this._currentFrame = 0;
                        if ( !_this._loop ) {
                            _this._play = false;
                        } else {
                            console.log( 'loop' );   
                        }   
                    }
                   
                }
                
            };

            this._playNode = playNode;
            // Todo: should not set automatically
            playNode.connect( ctx.destination );
        };

        /**
        * when working with realtime audio, it could be usefull to record a snipped and process on the fly
        * @private
        */
        PhaseVocoder.prototype._initRecNode = function () {
            var _this       = this;
            var ctx         = this._audioContext;
            var recData     = this._recBuff.getChannelData( 0 );
            var recHead     = 0;
            var recLen      = recData.length;
            var windowSize  = this._windowSize;

            var recNode     = ctx.createScriptProcessor( windowSize, 1, 1 );
            recNode.onaudioprocess = function ( e ) {
                var input   = e.inputBuffer.getChannelData( 0 );
                var output  = e.outputBuffer.getChannelData( 0 );
                var i;
                if ( !_this._record ) {
                    // pass
                    for ( i = 0; i < windowSize; i++ ) {
                        output[ i ] = input[ i ];
                    }
                } else {
                    for ( i = 0; i < windowSize; i++ ) {
                        recData[ recHead + i ] = input[ i ];
                    }

                    recHead += windowSize;
                    if ( recHead >= recLen ) {
                        _this._record = false;
                        recHead = 0;
                        _this.createSTFT( recData );
                        return;
                    }
                }
            };
            this.input = recNode;  
        };

        /**
        *   Creates a short time fourier transformation on supplied buffer
        *   This basically initialize the PhaseVocoder's phase and amp buffer
        *   @param {Float32Array} arrayBuffer The buffer to perform the STFT on
        *   @returns {PhaseVocoder} Self reference for chaining
        */
        PhaseVocoder.prototype.createSTFT = function( arrayBuffer ) {
            var ctx = this._audioContext;
            var win = this.hanningWindow;
            var hop = this._hopSize;

            var windowSize      = this._windowSize;
            var fft             = new FFT( windowSize, ctx.sampleRate );
            var phaseDeltas     = new Float32Array( windowSize );

            var phaseBuff       = this._phaseBuff;
            var ampBuff         = this._ampBuff;
        
            var phaseOffsets    = [ 0, HALF_PI ];
            var len = arrayBuffer.length - windowSize;

            for ( var i = 0, frameNum = 0; i < len; i+= hop, frameNum++ ) {
           
                var j;
                var frame = new Float32Array( windowSize );
                var phaseOffset = phaseOffsets[ frameNum % phaseOffsets.length ];
                // windowing
                for ( j = 0; j < windowSize; j++ ) {

                    frame[ j ] = arrayBuffer[ i + j ] * win[ j ];
   
                }

                // to fft
                fft.forward( frame );

                //
                for ( j = 0; j < windowSize; j++ ) {

                    var real  = fft.real[ j ];
                    var imag  = fft.imag[ j ];

                    var polar = carToPol( real, imag );
                    var amp   = polar[ 0 ];
                    var phase = polar[ 1 ];
                    //phase
                    var phaseDelta = phase - phaseDeltas[ j ]; 

                    // wrap phase between -pi and pi
                    phaseDelta = phaseDelta - 2 * Math.PI * Math.floor( phaseDelta / TWO_PI + 0.5 );  

                    var bufferPos = frameNum * windowSize + j;
                    // store in buffers
                    phaseBuff[ bufferPos ] = phaseDelta;
                    ampBuff  [ bufferPos ] = amp;

                    phaseDeltas[ j ] = phase;
                }

            }

            return this;

        };

        /**
        *   Trigger recording. Be aware though, recording is bound to the frame rate (windowsize)
        *   So recording will not be emmidiate
        *   @param {AudioNode} sourceNode The source to record (will be connected and routed to an internal record node)
        *   @returns {PhaseVocoder} Self reference for chaining
        */
        PhaseVocoder.prototype.record = function ( sourceNode ) {
            if ( ! this._record ) {
                if ( ! this.input ) {
                    this._initRecNode();
                }

                sourceNode.connect( this.input );
                this._currentFrame = 0;
                this._record = true;
            }

            return this;
        };

        /**
        *   Playback (call record or createSTFT before this)
        *   @param {float} [speed=1]
        *   @param {boolean} [loop=false] 
        *   @returns {PhaseVocoder} Self reference for chaining
        */
        PhaseVocoder.prototype.play = function ( speed, loop ) {
            speed = speed === undefined ? 1 : speed;
            loop  = loop  === undefined ? false : true;

            this._loop = loop;
            this._currentFrame = 0;
            this._play = true;
            this.playbackRate = speed;

            return this;
        };

        /**
        *   Set / get progress
        *   @param {float} [progress=1] Set the progress of 0-1
        *   @returns {PhaseVocoder} Self reference for chaining
        */
        PhaseVocoder.prototype.progress = function ( progress ) {
            if ( progress !== undefined ) {
                this._currentFrame = Math.floor( progress * this._numFrames );
                this._currentFrame = Math.max( 0, Math.min( this._currentFrame, this._numFrames -1 ) );
            } else {
                return this._currentFrame / this._numFrames;
            }
            return this;
        };

        /**
        *   Connect audio out
        */
        PhaseVocoder.prototype.connect = function ( audioNode ) {
            return this._playNode.connect( audioNode );
        };

        /**
        *   Disconnect audio out
        */
        PhaseVocoder.prototype.disconnect = function () {
            return this._playNode.disconnect();
        };

        /********************
        * "static" methods
        ********************/

        /**
        *   Utility function. Returns the number of frames needed to contain a X second buffer
        *   @param {float} seconds Todo...
        *   @returns {integer} The number of frames needed to contain the duration
        */
        PhaseVocoder.durationToFrames = function ( seconds, sampleRate, windowSize, hopSize ) {
            var overlapFactor = windowSize / hopSize;

            return Math.ceil( seconds * ( sampleRate / windowSize ) * overlapFactor );
        };

    return PhaseVocoder;
}));