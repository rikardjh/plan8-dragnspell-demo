/**
*   File: ResourceLoaderAudio.js
*   Documentation: jsdoc
*
*   ResourceLoaderAudio
*   Todo: Move API into a ResourceLoader prototype
*         Should be an API object instead of psedu-class?
*         Add callback for progression
*
*   @author Rikard Lindström <rikardjh@gmail.com>
*/

define ( function () {

    'use strict';

    /**
    * Internal scope constructor to be able to have multiple load calls on the same object
    * @constructor
    */
    var LoadScope = function ( urls, audioContext, onComplete ) {
        this.urls               = urls || [];
        this.resourceList       = [];
        this.audioContext       = audioContext || new AudioContext();
        this.urlMapping         = {};
        this.loaded             = false;
        this.loadCount          = 0;
        this.onComplete         = onComplete;
    };

    /**
    * Exposed constructor
    * @constructor
    */
    var ResourceLoaderAudio = function ( options ) {
        //this._mainScope = new Scope( options.urls, options.audioContext );
        this.options = options;
    };

    /*** Setup prototype ***/
    var p = ResourceLoaderAudio.prototype;

    /*** private methods ***/
    p._onAllComplete = function ( scope ) {
        scope.loaded = true;

        if ( scope.onComplete ) {
            var args = [];
            for ( var i = 0; i < scope.resourceList.length; i++ ) {
                args.push( scope.resourceList[ i ].buffer );
            }
            args.push( scope.audioContext );
            scope.onComplete.apply( this, args );
        }

    };

    p._onSingleComplete = function ( buffer, resObj, scope ) {

        scope.loadCount ++;

        resObj.buffer = buffer;
        resObj.loaded = true;

        if ( scope.loadCount === scope.urls.length ) {
            this._onAllComplete( scope );
        }
    };

    p._loadResource = function ( resObj, scope ) {
        
        var url          = resObj.url;
        var audioContext = scope.audioContext;

        var request = new XMLHttpRequest();

        request.open( 'GET', url, true );
     
        request.responseType = 'arraybuffer';

        var _this = this;

        request.onload = function () {
            // decode audio
            audioContext.decodeAudioData( request.response,
                function ( buffer ) {
                    if ( !buffer ) {
                        console.error ( 'error decoding audio data: ' + url );
                        return;
                    }

                    _this._onSingleComplete( buffer, resObj, scope );
                },
                function ( error ) {
                    console.error ( 'decodeAudioData error', error );
                }
            );
        };

        request.onerror = function () {
            console.error( 'ResourseLoader: XHR error: ' + url );
        };

        request.send();

        // var _this = this;
        // this.load( resource.url, audioContext, function ( buffer, ctx ) {
        //     resObj.loaded = true;
        //     resObj.buffer = buffer;
        //     _this._onSingleComplete();
        // } );
    };

    p._loadResources = function ( scope ) {

        for ( var i = 0; i < scope.resourceList.length; i++ ) {
            var resource = scope.resourceList[ i ];
            if ( !resource.loaded ) {
                this._loadResource( resource, scope );
            }
        }

    };

    /*** API ***/

    /**
    * When loader is used in a procedual manner, this can be used to add resources one at a time
    * @param {string} url The url of an audio resource to add
    */
    p.addResource = function ( url ) {

        var resObj = {
            url : url,
            loaded : false
        };

        this.urlMapping[ url ] = resObj;
        this.resourceList.push( resObj ); 

        this._resourcesNotLoaded++;

        return this;

    };

    /**
    * Gets the mapped resource for a loaded asset.
    * @param {string} url The url previously used with .addResource
    */
    p.getResourceForUrl = function ( url ) {
        if ( this.urlMapping[ url ] && !this.urlMapping[ url ].loaded ) {
            throw 'Error, resource not loaded. Called load yet?';
        }
        return this.urlMapping[ url ] && this.urlMapping[ url ].buffer;
    };

    /**
    * Main load function, a call without urls will start loading the previously added resources.
    * If urls are supplied, these are scoped, added and loaded immediatly
    * @param {(string|string[])} [url_s] The urls of the resources to load
    * @param {AudioContext} [audioContext} The audio context on which to create the buffers 
    *                                    for the loaded audio. If not supplied, a new one is created
    * @param {function} [onComplete] Callback for when everything is loaded
    */
    p.load = function ( url_s, audioContext, onComplete ) {
        var scope;

        if ( typeof url_s === 'string' ) {
            url_s = [ url_s ];
        }


        if ( url_s === undefined ) {
            // use main scope (this)
            scope = this; 
        } else {
            // use scope object to be able to handle subsequent calls
            scope = new LoadScope( url_s, audioContext, onComplete );

            // add individual resources
            for ( var i = 0; i < url_s.length; i++ ) {
                this.addResource.call( scope, url_s[ i ] );
            }
        }
        
        this._loadResources( scope );

        return this;
    };


    // return constructor
    return ResourceLoaderAudio;
});