/**
* file: main.js
* author: Rikard Lindström <rikardjh@gmail.com>
*/

require.config( {

    shim : {
        'FFT' : {
            exports : 'FFT'
        }
    },

	paths : {
		'jquery'          : '../../vendors/jquery-1.10.2',
        'FFT'             : '../../vendors/dsp'
	},

    urlArgs : "bust="+new Date().getTime() // prevent caching

} );

define( [ 'app', 'polyfill' ], function ( app ) {
	// initialize
	app.initialize( '#main' );

} );