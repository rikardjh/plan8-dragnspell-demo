define( [ 'chai', 'lib/PhaseVocoder' ], function ( chai, PhaseVocoder ) {
    var expect = chai.expect;

    describe( 'PhaseVocoder', function () {
        
        it( 'should be exported', function () {
            expect( PhaseVocoder ).to.be.ok;
        });


        describe( '.durationToFrames', function () {

            it ( 'should convert a duration in seconds to number of frames based on current audio context', function () {
               
                // one second, 44100 samples / sec, window size 1024, hop size 512 (overlap factor of 2)
                expect( PhaseVocoder.durationToFrames( 1, 44100, 1024, 512 ) ).to.equal( 87 );
            });
        });

    });
});