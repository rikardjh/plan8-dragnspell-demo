define( [ 'chai', 'app' ], function ( chai, app ) {
    var expect = chai.expect;

    describe( 'app', function () {
        
        it( 'should be exported', function () {
            expect( app ).to.be.ok;
        });

        it( 'should have an initialize method', function () {
            expect( app.initialize ).to.be.a( 'function' );
        });

    });
});