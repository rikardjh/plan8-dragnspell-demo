require.config({

    baseUrl: "../src/js",
    
    shim : {
        'FFT' : {
            exports : 'FFT'
        }
    },

    paths: {
        chai            : '../../node_modules/chai/chai',
        specs           : '../../tests/specs',
        jquery          : '../../vendors/jquery-1.10.2',
        FFT             : '../../vendors/dsp'
    },
    
    //urlArgs : 'bust=' + (new Date()).getTime()

});

define( [ 'require', 'chai' ], function ( require, chai ) {
    
    mocha.setup( 'bdd' ); 

    require([
        'specs/app.spec',
        'specs/PhaseVocoder.spec'
    ], function() {

        if ( typeof mochaPhantomJS !== "undefined" ) { 

            mochaPhantomJS.run(); 
        } else { 
           
            mocha.run(); 
        }
    });
});